function install_aur_packages {
    AUR_BASE="https://aur.archlinux.org/"
    SKYPE="skypeforlinux-stable-bin"

    echo -e "${STAGE_START}Install AUR packages${STAGE_END}"

    if [ ! -d "${HOME}/build" ]; then
        mkdir "${HOME}/build"
    fi

    pushd "${HOME}/build"
    if [ ! -d ${SKYPE} ]; then
        git clone "${AUR_BASE}${SKYPE}.git"
    fi
    popd

    fish -c "update-aur -a build -f -y --password=${PASS}"
    fish -c "type -q skypeforlinux" || fail "Skype for linux was not correctly installed"
}