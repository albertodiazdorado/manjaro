function create_keyboard_shortcuts {
    echo -e "${STAGE_START}Create keyboard shortcuts${STAGE_END}"

    xfce_dir="${HOME}/.config/xfce4/xfconf/xfce-perchannel-xml"
    xfce_file="xfce4-keyboard-shortcuts.xml"

    if [[ ! -f "${xfce_dir}/${xfce_file}" ]]; then
        echo -e "$WARNING Configuration file ${xfce_dir}/${xfce_file} does not exist. Skipping the keyboard configuration..."
        return
    fi

    temp_file=temp.xml
    parent_node="/channel/property/property[@name='custom']"
    node_without_name="/channel/property[@name='commands']/property[@name='custom']/property[not(@name)]"
    firefox_node="/channel/property[@name='commands']/property[@name='custom']/property[@value='firefox']"
    terminal_node="/channel/property[@name='commands']/property[@name='custom']/property[@value='xfce4-terminal']"
    
    pushd ${xfce_dir}
    
    echo -e "\nCreating keyboard shortcuts..."
    xmlstarlet ed -d ${firefox_node} -d ${terminal_node}					\
    -s ${parent_node} -t elem -n property 									\
    -i ${node_without_name} -t attr -n value -v "firefox" 					\
    -i ${firefox_node} -t attr -n type -v "string" 							\
    -i ${firefox_node} -t attr -n name -v "&lt;Primary&gt;&lt;Shift&gt;g"	\
    -s ${parent_node} -t elem -n property 									\
    -i ${node_without_name} -t attr -n value -v "xfce4-terminal"			\
    -i ${terminal_node} -t attr -n type -v "string"							\
    -i ${terminal_node} -t attr -n name -v "&lt;Primary&gt;&lt;Shift&gt;n"	\
    ${xfce_file} | sed 's/\&amp;/\&/g' > ${temp_file}
    
    rm ${xfce_file}
    mv ${temp_file} ${xfce_file}
    popd
}

