function install_fish_shell {
    echo -e "${STAGE_START}Install fish shell${STAGE_END}"
    echo ${PASS} | sudo -S pacman -S fish --noconfirm
    
    if test $(which fish); then
        echo ${PASS} | chsh -s "/usr/bin/fish"
    else
        fail "${ERROR_START}Could not find the fish.${ERROR_END}"
    fi

    grep $LOGNAME /etc/passwd | grep fish || fail "${ERROR_START}Fish is not the default shell${ERROR_END}"
}

function install_fish_functions {
    echo -e "${STAGE_START}Install custom fish functions${STAGE_END}"
    
    REMOTE_URL="https://gitlab.com/albertodiazdorado/fish_functions.git"
    CLONE_FOLDER=$(cat /proc/sys/kernel/random/uuid)
    git clone ${REMOTE_URL} ${CLONE_FOLDER}
    
    FISH_FUNCTIONS_FOLDER="${HOME}/.config/fish/functions"
    if test ! -d ${FISH_FUNCTIONS_FOLDER}; then
        mkdir -p ${FISH_FUNCTIONS_FOLDER}
    fi
    
    cp ${CLONE_FOLDER}/functions/* ${FISH_FUNCTIONS_FOLDER}
    rm -rf ${CLONE_FOLDER}

    # Verify installation
    fish -c "type -q update-aur" || fail "${ERROR_START}update-aur is not installed${ERROR_END}"
}

function install_omf {
    if ! fish -c "type -q omf"; then
        echo -e "${STAGE_START}Install OMF${STAGE_END}"
        TEMP_FILE=$(mktemp)
        curl -L https://get.oh-my.fish > ${TEMP_FILE}
        fish ${TEMP_FILE} --noninteractive
        rm ${TEMP_FILE}
    fi

    # Verify installation
    fish -c "type -q omf" || fail "${ERROR_START}Could not install oh-my-fish${ERROR_END}"
}

function customize_omf {
    echo -e "${STAGE_START}Customize omf plugins${STAGE_END}"

    fish -c "omf update" || fail "${ERROR_START}omf update failed${ERROR_END}"
    if ! fish -c "omf l | grep agnoster" > /dev/null; then
        fish -c "omf install agnoster" || fail "${ERROR_START}omf install agnoster failed${ERROR_END}"
    fi
    fish -c "omf theme agnoster" || fail "${ERROR_START}omf theme agnoster failed${ERROR_END}"
}
