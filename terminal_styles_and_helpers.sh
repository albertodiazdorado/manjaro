#!/bin/sh

ERROR_START="\033[1;31mError: "                     # red, bold
ERROR_END="\033[0m"                                 # red, bold
WARNING="\033[1;33mWARNING:\033[0m"                 # yellow, bold
BOLD_START="\033[1m"
BOLD_END="\033[0m"
INFO="${BOLD_START}INFO:${BOLD_END}"                # white, bold
HEADING_START="\n\033[1m"
HEADING_END="\n-----------------------\033[0m"
STAGE_START="\033[1;32m=> "                          # green, bold
STAGE_END="\033[0m"                                 # green, bold


function fail() {
    echo -e "${ERROR_START}""$@""${ERROR_END}"
    exit 1
}