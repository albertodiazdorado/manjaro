FROM manjarolinux/base:latest

COPY . /manjaro
WORKDIR /manjaro

RUN pacman -Syu --noconfirm
RUN pacman -S sudo --noconfirm
RUN useradd -m enanone || exit 1
RUN echo "enanone:enanonePassword" | chpasswd
RUN echo "enanone  ALL=(ALL:ALL) ALL" >> /etc/sudoers
