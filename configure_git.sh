function configure_git {
    echo -e "${STAGE_START}Create git user and git shortcuts${STAGE_END}"
    
    git config --global user.email "albertodiazdorado@gmail.com"
    git config --global user.name "Alberto"
    
    git config --global alias.co checkout
    git config --global alias.st status
    git config --global alias.f fetch
    git config --global alias.alias "! git config --get-regexp ^alias\. | sed -e s/^alias\.// -e s/\ /\ =\ /"
    git config --global alias.del '! git branch | egrep -v "(^\*|master|dev)" | xargs git branch -D; git remote prune origin'

    # Verify success
    git alias || fail "${ERROR_START}git alias is not defined${ERROR_END}"
    
    create_ssh_keys
}

function create_ssh_keys {
    echo -e "${STAGE_START}Generate ssh keys${STAGE_END}"
    pushd ${HOME}
    if [[ -f  ".ssh/id_rsa" ]] && [[ -f ".ssh/id_rsa.pub" ]]; then
        echo -e "\n${WARNING} Default ssh-keys already exist. Skipping this step..."
        return
    else
        test -d ".ssh" && rm -r ".ssh"
        mkdir ".ssh"
        ssh-keygen -f ".ssh/id_rsa" -t rsa -N ""
    fi

    # Verify key are created succesfully
    test -f ".ssh/id_rsa" && test -f ".ssh/id_rsa.pub" || \
        fail "${ERROR_START}Something went wrong when creating ssh keys${ERROR_END}"

    popd
}
