function update_system {

    if test -f /.dockerenv; then
        echo -e ${WARNING} "Running in a container, skipping entropy generator"
    else
        echo -e ${STAGE_START}Create ENTROPY${STAGE_END}
        echo ${PASS} | sudo -S pacman -Sy haveged --noconfirm 2> /dev/null
        echo ${PASS} | sudo -S systemctl start haveged 2> /dev/null
        echo ${PASS} | sudo -S systemctl enable haveged 2> /dev/null
    fi

    echo -e ${STAGE_START}Update mirrors${STAGE_END}
    echo ${PASS} | sudo -S pacman-mirrors --geoip 2> /dev/null

    echo -e ${STAGE_START}Update system${STAGE_END}
    echo ${PASS} | sudo -S pacman -Syyu --noconfirm 2> /dev/null

    echo -e ${STAGE_START}Install packages${STAGE_END}
    echo ${PASS} | sudo -S pacman -S base-devel --needed --noconfirm
    echo ${PASS} | sudo -S pacman -S xfce4 docker git xclip xmlstarlet \
                                     openssh npm firefox vscode        \
                                     texlive-most --needed --noconfirm \
                                     2> /dev/null
}

function vs_code_post_install {
    echo -e "${STAGE_START}Customize VS code${STAGE_END}"
    code --install-extension James-Yu.latex-workshop || fail "Failed to install VS code latex workshop"
    code --install-extension yzhang.markdown-all-in-one || fail "Failed to install VS code markdown"
    code --install-extension foxundermoon.shell-format || fail "Failed to install VS code shell format"
    code --install-extension bmalehorn.vscode-fish || fail "Failed to install VS code fish IDE"
    code --install-extension donjayamanne.githistory || fail "Failed to install git history"

    vscode_settings='{ "terminal.integrated.fontFamily": "Meslo LG S for Powerline" }'
    echo $vscode_settings > "${HOME}/.config/Code/User/settings.json"

    FONTS_CLONE_FOLDER=$(cat /proc/sys/kernel/random/uuid)
    git clone https://github.com/powerline/fonts.git --depth=1 ${FONTS_CLONE_FOLDER} || fail "Failed to clone powerline fonts"
    pushd ${FONTS_CLONE_FOLDER}
    ./install.sh || fail "Failed to install powerline fonts"
    popd
    rm -rf ${FONTS_CLONE_FOLDER}
}