#!/bin/sh
DIR="$(dirname "$(realpath "$0")")"

if [ "$#" -lt 1 ]; then
    echo "Missing password. Run this script as ./manjaro.sh \$password"
    exit 1
fi
PASS=$1

source "${DIR}/terminal_styles_and_helpers.sh" || exit 1

source "${DIR}/update_system.sh" || fail "Failed to import update_system.sh"
source "${DIR}/create_keyboard_shortcuts.sh" || fail "Failed to import create_keyboard_shortcuts.sh"
source "${DIR}/create_custom_makepkg.sh" || fail "Failed to import create_custom_makepkg.sh"
source "${DIR}/fish_shell.sh" || fail "Failed to import fish_shell.sh"
source "${DIR}/configure_git.sh" || fail "Failed to import configure_git.sh"
source "${DIR}/install_common_AUR_packages.sh" || fail "Failed to import install_common_AUR_packages.sh"

pushd ${HOME}

echo -e "${HEADING_START}MANJARO CUSTOMIZATION${HEADING_END}"
update_system
vs_code_post_install

create_keyboard_shortcuts

install_fish_shell
install_fish_functions
install_omf
customize_omf

configure_git

create_custom_makepkg

install_aur_packages



