Customizes a fresh manjaro installation to my heart's content.
Check out the [Features](#features) below to see what is installed.

This script is [CI-tested](#testing).
Hence, if you execute this script on a fresh manjaro installation, you will always end up with the same configuration bar package updates.

# Usage
## New system
On a fresh manjaro linux installation, download the repository and execute
it passing your user password as the first argument
```sh
git clone https://gitlab.com/albertodiazdorado/manjaro.git
pushd manjaro
bash manjaro $myPassword
popd
```
## Existing system
These scripts are idempotent - to an extent.
You can execute them on an existing manjaro installation, and they will make sure that all the features are installed. 

# Features
1. Update pacman mirros, so that you are guaranteed to use those closest to your location
2. Global system update (may take a while)
3. Install basic packages:
   * docker
   * npm
   * firefox
   * openssh
   * vs code
   * ...and some utilities
4. Install plugins for vs code
   * Latex workshop
   * Markdown
   * Shell (bash)
   * Fish
   * Git history 
5. Adds keyboard shortcuts:
   * Open the terminal via `CTRL+SHIFT+N`
6. Install fish shell, make it the login shell, install [custom fish functions](https://gitlab.com/albertodiazdorado/fish_functions) and install oh my fish (with the agnoster plugin)
7. Configure git with the email `albertodiazdorado@gmail.com` and create ssh keys
8. Customize `makepkg` so that it is slightly more performant
9.  Install packages from the AUR repository
   * (empty at the moment)

# Testing
## CI
Every push to master will trigger a CI pipeline in Gitlab

## Local
To test locally, make sure that you have docker installed and enabled.
Then, you can run the tests via script:
```sh
bash local-test.sh
```
You can also do it manually, by building the image and starting the container with the appropriate command:
```sh
docker build -t manjaro-installation .
docker run manjaro-installation su enanone -c "bash manjaro.sh enanonePassword"
```

## Notes
The password `enanonePassword` must be consistent.
This applies to `.gitlab-ci.yaml`, but also to `Dockerfile` and the command issued to the container.