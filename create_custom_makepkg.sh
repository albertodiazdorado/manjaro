function create_custom_makepkg {
    MAKEPKG_FILE="${HOME}/.makepkg.conf"
    
    echo -e "${STAGE_START}Create custom makepkg.conf${STAGE_END}"
    
    cp /etc/makepkg.conf ${MAKEPKG_FILE}
    
    sed -i 's|-march=x86-64|-march=native|g' ${MAKEPKG_FILE}
    sed -i 's|CXXFLAGS=.*|CXXFLAGS="${CFLAGS}"|g' ${MAKEPKG_FILE}
    sed -i 's|#MAKEFLAGS="-j2"|MAKEFLAGS="(nproc)"|g' ${MAKEPKG_FILE}
}
